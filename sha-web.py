from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def control_panel():
    #render_template imports in html files from the templates folder
#    return render_template('control_panel.html')
    return render_template('control_panel.html', duration=request.args['duration'])

if __name__ == '__main__':
    app.run(debug=True, port=80, host='0.0.0.0')
