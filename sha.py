from flask import Flask, render_template, request
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)#set pin mapping scheme
GPIO.setup(13, GPIO.OUT)#define pins as outputs
GPIO.setup(19, GPIO.OUT)#these pins are 33,35 in the "BOARD" mapping scheme

in1 = GPIO.PWM(13,2000)
in2 = GPIO.PWM(19,2000)
#dc = 20


def mStop():
    in1.stop()
    in2.stop()
def mRight(dutyCycle, duration):
    in1.start(dutyCycle)
    time.sleep(duration)
    mStop()

def mLeft(dutyCycle, duration):
    in2.start(dutyCycle)
    time.sleep(duration)
    mStop()


app = Flask(__name__)

@app.route('/')
def control_panel():
    #render_template imports in html files from the templates folder
    args = request.args
    print(args)
    if 'duration' and 'direction' and 'motor speed' in args:
        if args['motor speed'] != '':
            dc = int(args['motor speed'])
            print('motor-speed is in args')
            print(dc)
        else:
            dc = 20
        dur = int(args['duration'])
        dir = args['direction']
        print('all arguments provided, moving now')
        print('duration = '+str(dur)+' direction = '+dir+' motor-speed = '+str(dc))
#        print(type(dur))
        if dir == 'right':
            print('moving right for '+str(dur)+' seconds')
            mRight(dc,dur)
            print('move complete')
        if dir == 'left':
            print('moving left for '+str(dur)+' seconds')
            mLeft(dc,dur)
            print('move complete')

    else:
        print('no arguments provided')


    print(request.args)
    return render_template('control_panel.html')

if __name__ == '__main__':
    app.run(debug=True, port=81, host='0.0.0.0')
