import RPi.GPIO as GPIO
import time
import keyboard

GPIO.setmode(GPIO.BCM)#set pin mapping scheme
GPIO.setup(13, GPIO.OUT)#define pins as outputs
GPIO.setup(19, GPIO.OUT)#these pins are 33,35 in the "BOARD" mapping scheme

in1 = GPIO.PWM(13,2000)
in2 = GPIO.PWM(19,2000)
dc = 90

def forward(dutyCycle):
    in1.start(dutyCycle)

def reverse(dutyCycle):
    in2.start(dutyCycle)
def mStop():
    in1.stop()
    in2.stop()

if __name__ == "__main__":
    try:
        while 1:
            if keyboard.is_pressed('up') and dc < 100:
                dc = dc + 5
                in1.ChangeDutyCycle(dc)
                in2.ChangeDutyCycle(dc)
            if keyboard.is_pressed('down') and dc > 0:
                dc = dc - 5
                in1.ChangeDutyCycle(dc)
                in2.ChangeDutyCycle(dc)
            if keyboard.is_pressed('left'):
                mStop()
                reverse(dc)
            if keyboard.is_pressed('right'):
                mStop()
                forward(dc)
            time.sleep(1)
            print(dc)
    except KeyboardInterrupt:
        pass
    mStop()
    GPIO.cleanup()



